
const dbService = {
  //baseUrl: 'http://138.197.128.236:8081/ws-rest/servicios'
  baseUrl: 'https://ahorraseguros.mx/ws-rest/servicios',
  baseAutos: 'https://ahorraseguros.mx/ws-autos/servicios'
}

export default dbService
